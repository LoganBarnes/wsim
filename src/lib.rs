pub mod framework;
pub mod sim;

use winit::event_loop::EventLoop;

#[cfg(target_arch = "wasm32")]
use winit::dpi::PhysicalSize;

#[allow(dead_code)]
pub fn run<E: framework::Example>(title: &str) {
    let event_loop = EventLoop::new();
    let builder = winit::window::WindowBuilder::new().with_title(title);
    let app_window = builder.build(&event_loop).unwrap();

    #[cfg(not(target_arch = "wasm32"))]
    {
        env_logger::init();
        futures::executor::block_on(framework::run_async::<E>(event_loop, app_window));
    }
    #[cfg(target_arch = "wasm32")]
    {
        use winit::platform::web::WindowExtWebSys;

        web_sys::window()
            .and_then(|win| win.document())
            .and_then(|doc| doc.get_element_by_id("canvas-wrapper"))
            .and_then(|canvas_wrapper| {
                app_window.set_inner_size(PhysicalSize {
                    width: canvas_wrapper.client_width(),
                    height: canvas_wrapper.client_height(),
                });

                canvas_wrapper
                    .append_child(&web_sys::Element::from(app_window.canvas()))
                    .ok()
            })
            .expect("Could not append canvas to '#canvas-wrapper'");

        //todo: implement web_window.onresize() callback
        // https://dev.to/deciduously/reactive-canvas-with-rust-webassembly-and-web-sys-2hg2
        // https://rustwasm.github.io/wasm-bindgen/examples/closures.html
        // Or wait for: https://github.com/rust-windowing/winit/issues/1491

        wasm_bindgen_futures::spawn_local(framework::run_async::<E>(event_loop, app_window));
    }
}

#[cfg(target_arch = "wasm32")]
mod wasm {
    use wasm_bindgen::prelude::*;

    // This is like the `main` function, except for JavaScript.
    #[wasm_bindgen(start)]
    pub fn main_js() -> Result<(), JsValue> {
        std::panic::set_hook(Box::new(console_error_panic_hook::hook));
        console_log::init_with_level(log::Level::Debug).expect("could not initialize logger");

        log::debug!("Starting app");

        super::run::<crate::sim::Sim>("WGPU Test");
        Ok(())
    }
}
