#!/usr/bin/env bash

glslangValidator --version >/dev/null 2>&1 || {
  echo >&2 "'glslangValidator' command not found. Aborting."
  exit 1
}

for filename in glsl/*; do
  glslangValidator -V "$filename" -o "spir-v/$(basename "$filename").spv"
done
