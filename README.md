# WGPU Test

## Native Application

### Dependencies

* [rust][rust-link]

### Run

```sh
cargo run
```

## Browser Application

Requires a "bleeding edge" browser with the [unsafe WebGPU][browsers-link] flags enabled.

The current web page can be viewed [here][wsim-link] (with the above browser settings enabled).

### Dependencies

* [wasm-pack][wasm-pack-link]
* [node.js][node-link]*
* [yarn][yarn-link]* (optional alternative to [npm][npm-link])

\*node.js can be installed and managed with [nvm][nvm-link].

### Install

```sh
yarn
```

### Run in debug mode

```sh
# Builds the project and opens it in a new browser tab. Auto-reloads when the project changes.
yarn start
```

### Build in release mode

```sh
# Builds the project and places it into the `dist` folder.
yarn build
```

[rust-link]: https://www.rust-lang.org/tools/install

[browsers-link]: https://github.com/gpuweb/gpuweb/wiki/Implementation-Status
[wsim-link]: https://loganbarnes.gitlab.io/wsim

[wasm-pack-link]: https://rustwasm.github.io/wasm-pack/installer/ 
[nvm-link]: https://github.com/nvm-sh/nvm#installing-and-updating
[node-link]: https://nodejs.org/en/
[yarn-link]: https://classic.yarnpkg.com/en/docs/install
[npm-link]: https://www.npmjs.com/get-npm
